import math

# Решение линейного уравнения
# a*x + b = 0
def line (a, b):
    if not a == 0:
        return [-b / a]
    return []

# Решение квадратного уравнения
# a*x^2 + b8x + c = 0
def square (a, b, c):
    if a == 0:
        return line(b, c)
    D = b ** 2 - 4 * a * c
    if D == 0:
        return [-b / (2*a)]
    elif D > 0:
        x1 = (-b + math.sqrt(D)) / (2 * a)
        x2 = (-b - math.sqrt(D)) / (2 * a)
        return [x1, x2]
    return []

# Решение кубического уравнения
# Слишком сложно, не могу отвечать за правильность
# a*x^3 + b*x^2 + c*x + d = 0
def cube (a, b, c, d):
    if a == 0:
        return square(b, c, d)
    if d == 0:
        return square(a, b, c).append(0)
    n1 = b/a
    n2 = c/a
    n3 = d/a
    q = (n1**2 - n2*3)/9
    r = (n1*(2*(n1**2) - 9*n2) + 27*n3)/54
    r2 = r**2
    q3 = q**3
    if r2 < q3:
        t = math.acos(r / math.sqrt(q3))
        n1 /= 3
        q = -2 * math.sqrt(q3)
        x1 = q * math.cos(t/3) - n1
        x2 = q * math.cos((t + 2 * math.pi)/3) -n1
        x3 = q * math.cos((t - 2 * math.pi)/3) -n1
        return [x1, x2, x3]
    else:
        r = math.fabs(r)
        y = - math.pow(r + math.sqrt(r2 - q3), 1/3)
        if not y == 0:
            z = q / y
        else:
            z = 0
        n1 /= 3
        f = y + z
        g = y - z
        x1 = f - n1
        x2 = (-0.5)*f - n1
        x3 = (math.sqrt(3)/2)*math.fabs(g)
        return [x1, x2, x3]

def equal(a, b, c = None, d = None):
    if not c == None and not d == None:
        return cube(a, b, c, d)
    if not c == None:
        return square(a, b, c)
    return line(a, b)